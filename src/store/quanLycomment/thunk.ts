
import { createAsyncThunk } from '@reduxjs/toolkit'
import { quanLyAirbnbCommentServices } from 'services/quanlyAirbnbComment'
import { myCustomDataType } from 'types/QuanlyComment'

export const getAirbnbCommentThunk = createAsyncThunk(
    'getAirBnBcomment/CommentThunk',
    async (_, { rejectWithValue }) => {
        try {
            const data = await quanLyAirbnbCommentServices.getRoomCommentList()
            // sleep
            await new Promise((resolve)=> setTimeout(resolve, 1000))
            return data.data.content
        } catch (err) {
            return rejectWithValue(err)
        }   
    }
)
export const postCommentThunk = createAsyncThunk(
    'getAirBnBcomment/postCommentThunk',
    async (payload:myCustomDataType , { rejectWithValue }) => {
        try {
            const data = await quanLyAirbnbCommentServices.postComment(payload)
            return data.data.content
        } catch (err) {
            return rejectWithValue(err)
        }   
    }
)

