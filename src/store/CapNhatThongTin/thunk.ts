import { createAsyncThunk } from "@reduxjs/toolkit";
// import { LoginSchameType } from "schema/LoginSchame";
import { CapNhatThongTinServices } from "services";

export const capNhatThongTinThunk = createAsyncThunk(
  "CapNhatNguoiDung/ThayDoiThongTin", 
  async (param : {id:string,payload : any} , { rejectWithValue }) => { 
    try {
      const data = await CapNhatThongTinServices.capNhat(param.id,param.payload); 
      return data.data.content       
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const getThongTinUserThunk = createAsyncThunk(
    "CapNhatNguoiDung/LayThongTin", 
    async (id : any, { rejectWithValue }) => { 
      try {
        const data = await CapNhatThongTinServices.getThongTinUser(id); 
        return data.data.content       
      } catch (err) {
        return rejectWithValue(err);
      }
    }
  );
  export const uploadAnhThunk = createAsyncThunk(
    "CapNhatNguoiDung/UploadAnh", 
    async (file: any, { rejectWithValue }) => { 
      try {
        const data = await CapNhatThongTinServices.upLoadAnh(file); 
        return data.data.content       
      } catch (err) {
        return rejectWithValue(err);
      }
    }
  );