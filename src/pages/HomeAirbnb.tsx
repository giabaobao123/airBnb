import { SearchOutlined } from "@ant-design/icons";
import "animate.css";
import { Modal } from "antd";
import Footer from "components/layouts/Footer";
import { PATH } from "constant";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export const HomeAirbnb = () => {
  const navigate = useNavigate();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <section>
        <div>
          <div className="flex mt-20 lg:max-w-[90%] w-[85%] space-x-10 lg:mx-auto ml-[20px] ">
            <div className="w-1/2 max-tablet:w-[70%] items-center font-bold  flex  justify-center  ">
              <div className="">
                <h2 className=" pb-5 max-ip678:text-[20px] lg:text-3xl desktop:text-[40px] text-[25px] max-ip678Plus:text-2xl animate__animated animate__fadeInDown ">
                  Dive <br /> into Your Next <br /> Adventure with Airbnb
                </h2>
                <p className=" pb-5 text-gray-600 hidden lg:block max-lg:text-[16px] desktop:text-[20px] font-semibold animate__animated animate__fadeIn">
                  Unlock the world of exceptional apartment stays with Airbnb,
                  where extraordinary adventures await. <br />{" "}
                  <span className="hidden lg:inline">
                    Experience remarkable living spaces with just a click of a
                    button
                  </span>
                </p>
                <button
                  style={{
                    
                    color: "white",
                  }}
                  onClick={showModal}
                  className="mt-8  font-600 text-[13px] py-[16px] px-[36px]  outline-none items-center flex  rounded-xl animate__animated animate__fadeIn shadow-md text-white bg-mainColor hover:scale-110 transition-transform duration-300 ease-in-out "
                >
                  Find Unique
                </button>
                <Modal
                  title="Basic Modal"
                  open={isModalOpen}
                  onOk={handleOk}
                  onCancel={handleCancel}
                  footer={null}
                >
                  <div className="relative">
                    <input
                      type="text"
                      placeholder="Start your Search"
                      className="border-2 py-[12px]  rounded-3xl w-full pl-5 pr-10 shadow-md transition focus:ring-mainColor focus:border-mainColor"
                    />
                    <SearchOutlined className="absolute right-3 top-1/2 transform -translate-y-1/2 p-3 rounded-3xl bg-mainColor border-2 text-white" />
                  </div>
                </Modal>
              </div>
            </div>
            <div className="w-1/2 max-tablet:w-[60%]">
              <img
                className="w-[100vh] animate__animated animate__fadeInDown"
                src="https://i.ibb.co/dpM7M6w/banner-Home1.png"
                alt=".../"
              />
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className=" my-20 max-w-[90%] mx-auto rounded-xl  ">
          <h2 className=" my-5 text-3xl   max-ip678Plus:text-2xl  font-bold">
            Live anywhere
          </h2>
          <div
            onClick={() => {
              navigate(PATH.ListRoom);
            }}
          >
            <div className="grid grid-cols-2 lg:grid-cols-4 max-tablet:grid-cols-1 desktop:gap-[30px] gap-[20px] max-tablet:gap-[10px] cursor-pointer ">
              <div className="relative">
                <img
                  className="h-[250px] tablet:w-full max-tablet:w-full  max-h-full desktop:w-full  mb-2 rounded-xl shadow-md transform hover:scale-110 transition-transform duration-300 ease-in-out"
                  src="https://a0.muscache.com/im/pictures/miso/Hosting-51131248/original/8a12d2e1-d27b-44c7-b33f-1bc18918db5f.jpeg?im_w=720"
                  alt="../"
                />
                <div className="left-[10px] flex flex-col justify-between absolute bottom-[10px] text-[white]">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="mainColor"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="lucide lucide-heart"
                  >
                    <path d="M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z" />
                  </svg>
                  <p className="hover:text-mainColor duration-200 ">
                    Unique Views
                  </p>
                </div>
              </div>

              <div className="relative">
                <img
                  className="h-[250px]   tablet:w-full max-tablet:w-full   max-h-full desktop:w-full mb-2 rounded-xl shadow-md transform hover:scale-110 transition-transform duration-300 ease-in-out "
                  src="https://a0.muscache.com/im/pictures/miso/Hosting-48766928/original/01c3131c-c07f-4151-b72a-c908f0a3f2dc.jpeg?im_w=1200"
                  alt="../"
                />
                <div className="left-[10px] flex flex-col justify-between absolute bottom-[10px] text-[white]">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="mainColor"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="lucide lucide-heart"
                  >
                    <path d="M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z" />
                  </svg>
                  <p className="hover:text-mainColor duration-200 ">
                    Special Rooms
                  </p>
                </div>
              </div>
              <div className="relative">
                <img
                  className="h-[250px] desktop:w-full   tablet:w-full max-tablet:w-full  max-h-full mb-2 rounded-xl shadow-md transform hover:scale-110 transition-transform duration-300 ease-in-out"
                  src="https://a0.muscache.com/im/pictures/miso/Hosting-45051304/original/dc8ae9e2-b071-4323-9f87-0df718222e08.jpeg?im_w=1200"
                  alt="../"
                />
                <div className="left-[10px] flex flex-col justify-between absolute bottom-[10px] text-[white]">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="mainColor"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="lucide lucide-heart"
                  >
                    <path d="M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z" />
                  </svg>
                  <p className="hover:text-mainColor duration-200 ">
                    Cozy Retreats
                  </p>
                </div>
              </div>
              <div className="relative">
                <img
                  className="h-[250px] desktop:w-full   tablet:w-full max-tablet:w-full max-h-full mb-2 rounded-xl shadow-md transform hover:scale-110 transition-transform duration-300 ease-in-out"
                  src="https://a0.muscache.com/im/pictures/miso/Hosting-7312616/original/d29e524e-471c-444d-a5ee-421da93806f7.jpeg?im_w=1200"
                  alt="../"
                />
                <div className="left-[10px] flex flex-col justify-between absolute bottom-[10px] text-[white]">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="mainColor"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="lucide lucide-heart"
                  >
                    <path d="M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z" />
                  </svg>
                  <p className="hover:text-mainColor duration-200 ">
                    Luxury Apartments
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className=" my-20 max-w-[90%] mx-auto    ">
          <h2 className=" my-5 text-3xl max-ip678Plus:text-2xl font-bold ">
            Explore Intriguing Spots
          </h2>
          <div
            onClick={() => {
              navigate(PATH.ListRoom);
            }}
            className="grid lg:grid-cols-4  lg:gap-[10px] grid-cols-2  max-ip678Plus:gap-[10px] cursor-pointer"
          >
            <div className="flex flex-col items-start    ">
              <img
                className="h-[200px] hover:scale-110 animation duration-300 transition-all "
                src="https://i.postimg.cc/50Gnfpbj/Ho-Chi-Minh-City.png"
                alt="Ho-Chi-Minh-City"
              />
              <div className="ml-5  hover:text-mainColor duration-200 transition-all ">
                <button className="flex items-start  font-bold  ">
                  Ho Chi Minh city
                </button>
                <p className="text-gray-600 font-semibold  ">0.5 hour drive</p>
              </div>
            </div>
            <div className="flex items-start flex-col ">
              <img
                className="h-[200px] duration-300 transition-all hover:scale-110 animation "
                src="https://i.postimg.cc/y8VX42cv/PhuQuoc.png"
                alt="phu quoc"
              />
              <div className="ml-5  hover:text-mainColor duration-200">
                <p className="flex items-start font-bold ">Phu Quoc</p>
                <p className="text-gray-600 font-semibold">9.5 hour drive</p>
              </div>
            </div>
            <div className="flex items-start flex-col">
              <img
                className="h-[200px] duration-300 transition-all hover:scale-110 animation "
                src="https://i.postimg.cc/cJv3dWHM/HAnoi.png"
                alt="ha noi"
              />
              <div className="ml-5 hover:text-mainColor duration-200 ">
                <p className="flex items-start font-bold ">Ha Noi city</p>
                <p className="text-gray-600 font-semibold ">1 day</p>
              </div>
            </div>
            <div className="flex items-start flex-col">
              <img
                className="h-[200px] duration-300 transition-all hover:scale-110 animation "
                src="https://i.postimg.cc/6QVX22y9/dalat.png"
                alt="dalat"
              />
              <div className="ml-5 ">
                <p className="flex items-center font-bold hover:text-mainColor duration-200  ">
                  Đà Lạt
                </p>
                <p className="text-gray-600 font-semibold ">6.5 hour drive</p>
              </div>
            </div>
          </div>
          <div
            onClick={() => {
              navigate(PATH.ListRoom);
            }}
            className="grid lg:grid-cols-4 grid-cols-2 lg:gap-[10px] max-ip678Plus:gap-[0px] cursor-pointer "
          >
            <div className="flex flex-col items-start  ">
              <img
                className="h-[200px] duration-300 transition-all hover:scale-110 animation "
                src="https://i.postimg.cc/QCwPnhH4/cantho.png"
                alt=""
              />
              <div className="ml-5 hover:text-mainColor duration-200 ">
                <p className="flex items-start  font-bold ">Can tho</p>
                <p className="text-gray-600 font-semibold ">3.9 hour drive</p>
              </div>
            </div>
            <div className="flex flex-col items-start ">
              <img
                className="h-[200px] duration-300 transition-all hover:scale-110 animation "
                src="https://i.postimg.cc/4d3Sfmc2/nhatrang.png"
                alt="nha trang"
              />
              <div className="ml-5 hover:text-mainColor duration-200 ">
                <p className="flex items-start  font-bold ">Nha Trang</p>
                <p className="text-gray-600 font-semibold ">6.5 hour drive</p>
              </div>
            </div>
            <div className="flex flex-col items-start ">
              <img
                className="h-[200px] duration-300 transition-all hover:scale-110 animation "
                src="https://i.postimg.cc/3NT9yN0H/phan-thiet.png"
                alt="phan thiet"
              />
              <div className="ml-5 hover:text-mainColor duration-200 ">
                <p className="flex items-start  font-bold ">Phan Thiet</p>
                <p className="text-gray-600 font-semibold ">2.5 hour drive</p>
              </div>
            </div>
            <div className="flex flex-col items-start ">
              <img
                className="h-[200px]  duration-300 transition-all hover:scale-110 animation "
                src="https://i.postimg.cc/bNXdtbvn/DaNang.png"
                alt="dalat"
              />
              <div className="ml-5  hover:text-mainColor duration-200 ">
                <p className="flex items-start  font-bold ">Da Nang</p>
                <p className="text-gray-600 font-semibold ">15.5 hour drive</p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="p-[10px]">
        <div className="my-15  lg:w-full w-[95%]  max-ip678Plus:ml-[0px]  max-ip678Plus:w-full ml-[20px] shadow-md lg:mx-auto rounded-3xl h-96 overflow-hidden relative">
          <div className="h-full w-full relative">
            <img
              className="w-full h-full object-cover shadow-md"
              src="https://i.postimg.cc/cCfxm9Jn/Header-Banner.jpg"
              alt=".../"
            />

            <div className="absolute inset-0 flex text-left  lg:left-[100px] mx-auto w-[90%] lg:w-[80%]  text-white ">
              <div className="border-red-600 flex justify-around flex-col ">
                <div>
                  <p className="text-3xl max-ip678Plus:text-2xl font-bold text-left">
                    Try Hosting
                  </p>
                  <p className="text-lg max-ip678Plus:text-sm mt-4">
                    Discover Hosting with Airbnb. <br /> Share your space,
                    welcome travelers, and earn extra income. Start hosting
                    today
                  </p>
                </div>

                <button
                  style={{ backdropFilter: "blur(10px)" }}
                  className="mt-4 hover:translate-y-[-20%] px-6 py-3 font-600 shadow-md rounded-xl bg-transparent border-2 border-gray-300  text-white hover:bg-mainColor hover:border-mainColor duration-200 w-full"
                >
                  Learn More
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/*  */}

      <section>
        <div className="my-20 p-[20px] mx-auto  ">
          <div>
            <p className="text-3xl my-5 max-ip678Plus:text-2xl font-bold text-left">
              Discover things to do
            </p>
          </div>
          <div className="flex lg:flex-row items-baseline justify-between flex-col lg:space-x-[30px]  space-y-[30px]">
            {/* Experiences */}
            <div className="relative w-full">
              <img
                className="lg:h-80 h-full w-full rounded-3xl shadow-md"
                src="https://i.pinimg.com/564x/a3/e0/8d/a3e08d3c33f154caddeb2f86ed0ef98b.jpg"
                alt="../"
              />
              <button
                style={{ backdropFilter: "blur(10px)" }}
                className="absolute bottom-[10%]  left-1/2 -translate-x-1/2 mx-auto flex text-left flex-col justify-center items-center mt-52 border-2 w-[70%]  h-[50px] lg:h-20  shadow-md rounded-xl text-white border-gray-300 hover:bg-mainColor hover:border-mainColor duration-200"
              >
                <h2 className="text-xl  font-bold">Experiences</h2>
                <p className="font-semibold hidden ">
                  Find unforgettable activities near you
                </p>
              </button>
            </div>

            {/* online experiences */}
            <div className="relative w-full">
              <img
                className="lg:h-80 h-full w-full rounded-3xl shadow-md"
                src="https://i.pinimg.com/564x/a3/5a/3b/a35a3ba08c75d4263251ce5ca60e3cf0.jpg"
                alt="../"
              />
              <button
                style={{ backdropFilter: "blur(10px)" }}
                className="absolute bottom-[10%]  left-1/2 -translate-x-1/2 flex text-left flex-col justify-center items-center  mt-52 border-2 w-[70%]  mx-auto h-[50px] lg:h-20 m-auto shadow-md rounded-xl text-white border-gray-300 hover:bg-mainColor hover:border-mainColor duration-200"
              >
                <h2 className="text-[16px] font-bold">Online Experiences</h2>
                <p className="font-semibold hidden max-tablet::block">
                  Live, interactive activites led by host
                </p>
              </button>
            </div>

            {/* tip travel */}
            <div className="relative w-full">
              <img
                className="lg:h-80 h-full w-full rounded-3xl shadow-md"
                src="https://i.pinimg.com/564x/97/a2/c1/97a2c16090c0c64f284135f38d4a70a6.jpg"
                alt="../"
              />
              <button
                style={{ backdropFilter: "blur(10px)" }}
                className="absolute  bottom-[10%]   left-1/2 -translate-x-1/2 flex text-left flex-col justify-center items-center mt-52 border-2 w-[70%] h-[50px] lg:h-20 m-auto shadow-md rounded-xl text-white border-gray-300 hover:bg-mainColor hover:border-mainColor duration-200"
              >
                <h2 className="text-xl font-bold">Tip travel</h2>
                <p className="font-semibold hidden max-tablet::block">
                  Exploring Travel Hacks
                </p>
              </button>
            </div>
          </div>
        </div>
        <Footer />
      </section>
    </>
  );
};
